import os

from dotenv import load_dotenv

AUTHOR = 'Alejandro Garcia'
SITENAME = 'Hello I\'m Alejandro Garcia'
SITEURL = ""

PATH = "content"

TIMEZONE = 'Europe/Madrid'

DEFAULT_LANG = 'en'
# DISPLAY_PAGES_ON_MENU = True

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (
    ("Pelican", "https://getpelican.com/"),
    ("Python.org", "https://www.python.org/"),
    ("Jinja2", "https://palletsprojects.com/p/jinja/"),
    ("You can modify those links in your config file", "#"),
)

# Social widget
SOCIAL = (
    ("You can add links in your config file", "#"),
    ("Another social link", "#"),
)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True

THEME = './themes/zurb-F5-basic'

MARKDOWN = {
    "extension_configs": {
        "mdx_video": {},
    },
    "output_format": "html5",
}

# Load the .env file
load_dotenv()

AMPLITUDE_API_KEY = os.getenv("AMPLITUDE_API_KEY")
