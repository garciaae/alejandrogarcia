This is the repo for alejandrogarcia.me

    The site is built using Pelican: a static site generator, written in Python.

    The content is independent and written using Markdown syntax.
    Includes a simple command line tool to (re)generate site files
    Easy to interface with version control systems and web hooks
    Completely static output is simple to host anywhere

Features

    Chronological content (e.g., articles, blog posts) as well as static pages
    Site themes (created using Jinja2 templates)
    Publication of articles in multiple languages
    Generation of Atom and RSS feeds
    Syntax highlighting via Pygments
    Fast rebuild times due to content caching and selective output writing

* Check out [Pelican's documentation](http://docs.getpelican.com/en/stable/) for further information.
* The theme is based on [zurb-F5-basic](https://github.com/getpelican/pelican-themes/tree/master/zurb-F5-basic).
* Includes [pelicanfly](https://github.com/bmcorser/pelicanfly) for dealing with the icons.
